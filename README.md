# OpenML dataset: seattlecrime6

https://www.openml.org/d/42496

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: City of Seattle
**Source**: https://data.seattle.gov/Public-Safety/Crime-Data/4fs7-3vj5 - 24-06-2019
**Please cite**:   

This data represents crime reported to the Seattle Police Department (SPD). Each row contains the record of a unique event where at least one criminal offense was reported by a member of the community or detected by an officer in the field. This data is the same data used in meetings such as SeaStat (https://www.seattle.gov/police/information-and-data/seastat) for strategic planning, accountability and performance management. 

For more information see:
https://data.seattle.gov/Public-Safety/Crime-Data/4fs7-3vj5 For this version, the task was downsampled to 10 percent. Compute a new target Reported_Time. Compute new date features, ignore some features and encode as features as factor variables.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42496) of an [OpenML dataset](https://www.openml.org/d/42496). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42496/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42496/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42496/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

